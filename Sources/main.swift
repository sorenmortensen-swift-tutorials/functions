//
//  main.swift
//  Functions
//
//  Created by Søren Mortensen on 14/06/2017.
//  Copyright © 2017 Søren Mortensen. All rights reserved.
//

import Foundation

// --------------------------------------------------------------------------------------------------------------------
// FUNCTIONS & RETURN VALUES                                                         // MARK: Functions & Return Values
// --------------------------------------------------------------------------------------------------------------------

// You already know what functions are. In Pascal, though, there are also things called "procedures," and they're
// treated as though they're a different thing than functions. In Pascal, procedures perform some kind of operation but
// don't return a value at the end, whereas functions do.

// In normal usage, though, the name "function" applies to both things, and it's the same in Swift. You declare
// functions with the keyword `func`, and instead of showing whether they return a value or not by using a different
// name in each case, we just define it like this:

/// This function returns an `Int` value.
func returnsAnInt() -> Int {
    // .............^
    // This is called the "closure arrow" and you put the return type after it. It's just a hyphen `-` and an angle
    // bracket `>`. The word "closure" will make sense in the future, but for now just know that it basically means
    // function. You don't need to use the name "closure arrow," anyway.
    
    // In Swift, you return a value by writing `return` and then the value.
    return 7
    
    // In Pascal, you return a value by assigning the value to the name of the function, which is... frankly, stupid. In
    // Pascal, the previous line would have been written like this:
    //
    // ```
    // ReturnsAnInt := 7;
    // ```
    //
    // `return 7` is a lot clearer, isn't it?
}

/// This function doesn't return anything, and that's simply indicated by the fact that there's no closure arrow in the
/// function definition.
func returnsNothing() {
    
}

// You could also write the definition of `returnsNothing()` slightly differently. The type `Void`, also known as `()`
// (still read "void"), represents "nothing." So, because the function returns nothing, you could write it like this:

/// This function returns `Void`, which just means it doesn't return anything.
func alsoReturnsNothing() -> Void {
    
}

// We don't normally write `-> Void`, though, because it's clearer not to. If you don't write `-> Void`, it reminds you
// that you don't need a `return` statement in the function. It's okay if you forget, though - uncomment the body of
// this function and see what happens when you build the project:

// Hint: you can remove the comment from the selected line or lines by typing ⌘-/ (that's Command-slash). That will
// also add comments to selected lines if they aren't commented, and you can use it to toggle comments on sections of
// code.

func shouldReturnNothing() {
//    return 19
}

// Keep in mind, also, that the compiler will catch the mistake if you forget to return a value. Comment out the return
// statement in the `forgotToReturn()` function and see what the compiler does.

func forgotToReturn() -> String {
    return "Oops!"
}



// --------------------------------------------------------------------------------------------------------------------
// FUNCTION ARGUMENTS                                                                     // MARK: - Function Arguments
// --------------------------------------------------------------------------------------------------------------------

// None of the functions above, though, are going to do anything particularly interesting - they'll perform the same
// operations and return the same thing every time, so what's the point of writing them?

// In order to do interesting things with them, we need to be able to give them some information from outside that they
// can use. We do that by "passing arguments" to the functions.

// Don't worry, though, because this is going to be a lot simpler than it is in Pascal. Even though Swift does support
// some of the features that make function arguments confusing in Pascal, their use is highly discouraged and so I won't
// teach them to you yet. In addition to being confusing, they're not necessary for anything you could conceivably want
// to do, so don't worry about them.

// Let's start with something simple. Let's write a function that will add the ending "erino" to a word that you give
// to it, and print it out. We'll call it `stringerino`, in the spirit of what it's going to do.

// It's simple enough: this function has a single argument called `word`.

// When you "call" the function - which just means that you tell the compiler to run the code in the function - the
// value you give it for `word` gets copied inside the function. Note the fact that I said "copied" - a new String gets
// created inside the function, with whatever value you give it inside. You can then access that value by using the
// identifier `word`.

// By default, you can't change the value of `word` - it's a constant, as though you'd declared it with `let word =`.
// Even if you could change the value, it wouldn't affect the value of the original string that you passed in to the
// function - it's a copy, remember? It's a totally separate value inside the function than outside, stored in a
// different place in memory (despite the fact that the actual contents of the String are the same). I can't emphasise
// that enough.

// The function then creates a new string with the string "erino" on the end, and prints it out.

/// This function adds the ending "erino" onto a word `word`, and prints out the result.
func stringerino(word: String) {
    let stringerinoed = word + "erino"
    print(stringerinoed)
}

// To see it in action, uncomment the next few lines and run the program!

//stringerino(word: "Swift")
//stringerino(word: "Function")
//stringerino(word: "Return")

// Once you're done, comment the lines out again, because otherwise the printed output will clutter up anything we print
// in a later section of this file.

// There's something else important to note about function arguments in Swift. Let me show you a bit of Pascal code
// first.

// ```
// procedure PrintThreeNumbers(first: Integer; second: Integer; third: Integer);
// begin
//   WriteLn(IntToStr(first));
//   WriteLn(IntToStr(second));
//   WriteLn(IntToStr(third));
// end;
//
// begin
//   PrintThreeNumbers(5, 84, 1);
//   PrintThreeNumbers(8, 4, 2);
// end.
// ```

// Look at lines 138 and 139. In Pascal, when you call a procedure or function, you write its name, and then in
// parentheses () you list the values of the three arguments - called `first`, `second`, and `third`. It might be a bit
// confusing sometimes, trying to remember which argument is which. What if each of the arguments were a different type,
// and were used for different things? Like the function `Assign` - you use it to assign a String name to a File on
// disk. It takes one argument which is a string, and one which is a File. But do you remember which one comes first?
// You'd have to go and look at the declaration to find that out. It's a bit of a mess.

// However, in Swift, when you call a function, you put the names of each of the arguments into the call too! So, in
// Swift, the `assign` function might be declared like this:

func assign(name: String, toFile: FileWrapper) { }

// Then, when you call the function, it looks like this:

// I have to create a value to pass in for the second argument first, ignore this next line.
let file = FileWrapper(regularFileWithContents: "Save data goes here".data(using: .utf8)!)

// Here's the call to the function:
assign(name: "Save1", toFile: file)
// ....^ .............^
// Look at these! I guarantee you're never going to forget which way around the arguments go. You can even read the call
// to the function like a sentence: Assign the name "Save1" to the file `file`.

// You know what's even better? Xcode autocompletes everything for you, and its autocomplete is one of the best of any
// IDE out there. Try it! On the line after this comment, start typing "assign", and use the arrow keys (if you need to)
// to select the entry for the assign(name:toFile:) function. Then press enter, and Xcode will insert a call to the
// function right there for you. It'll put all the argument labels in, and you can press Tab to jump through the blue
// placeholders, and just type right over them.

// Try it here ^

// Want to see something else too? Before you delete your line of code (because it won't compile, unless you pass in
// the value `file` from above), hold Option and click on `assign`. This is called Quick Help, and it shows you the
// declaration of the function! So that's another way to remember the arguments.

// You'd never forget the order of the arguments! This is almost entirely unique to Swift, although I think the language
// Ruby has this as well (it's not required in Ruby, though, so almost nobody uses it). In everything else - C, C++,
// Java, Rust, Pascal, C#, Basic (I could go on and on) - the feature doesn't exist. The feature actually comes from
// Objective-C, which is the language that Swift comes from and is replacing. But Objective-C is old now, and isn't
// anywhere near as good as Swift in this day and age, and it's on its way out now that Swift is here.

// The upshot of all this is that Swift code is really easy, mentally, to read. It's why, when we called `stringerino`
// on lines 119-121, we put `word:` before the argument each time. You can try removing that, but if you do it won't
// compile.

// It's also why when we refer to functions in Swift, we write them with their argument labels included, like this:
// stringerino(name:)

// Look at the toolbar at the top of the source editor. There are a series of dropdown menus showing your current
// location, all the way from the project you're in (Functions) to the group (Sources) and file (main.swift) and even
// the location of your cursor (likely "No Selection"). If you click on No Selection, you'll open a dropdown menu that
// shows a list of everything defined in this file (this is a really helpful feature of Xcode). Notice that all the
// functions declared in the Function Arguments section are listed like this.

// This naming is particularly helpful, because you can declare multiple functions with the same name, but different
// arguments (it's called "function overloading"). That's perfectly fine, because the *full* name of the function isn't
// just the bit before the parentheses - it's that part plus all the argument names. Let me show you an example:

/// This function, called `stringerino(sentence:)`, is a different function than `stringerino(word:)`.
func stringerino(sentence: String) {
    for word in sentence.components(separatedBy: .whitespaces) {
        stringerino(word: word)
    }
}

// Can you figure out what that function does? Before you write a line of code that calls it, try figuring it out using
// these tools, because they're really helpful, and I can't encourage you enough to learn how to use them:

// 1. Read the function. A lot of the things in the function are really easily readable, and function calls often read
//    like English, so sometimes you can understand what's going on just by reading it.

// 2. Hold Option and click on things. All of the default code in the Swift standard library is well-documented, so
//    every function has some information in Quick Help, and it's usually very useful.

// 3. Hold Command and click on things. This will jump to the declaration of the thing, no matter where it is, and you
//    can look at the contents of the function if it's in the same module. So if you click on the call to
//    `stringerino(word:)`, it'll jump up to line 112 so you can look at the original function.

// 4. Finally, on the next line, insert a call to `stringerino(sentence:)` and run the program to print it out.

// Try it here ^

// There's one more thing about function arguments that's important to know about in Swift. Remember how I said that you
// can often read a function definition just like a sentence in English? Well, what if you have a function like this?

func add(number: Int, toNumber: Int) -> Int {
    return number + toNumber
    // ....^ .......^
    // This isn't very nice, is it? The two variables are now called `number` and `toNumber`, which really isn't very
    // descriptive.
}

// But look how nice and descriptive the call looks like from outside!

add(number: 3, toNumber: 9)

// Whatever will we do?

// The answer is internal argument names! If you put a second identifier in the function definition for an argument,
// that identifier becomes the name that you use for that variable on the inside of the function, and the first
// identifier is an external label for the argument, so everything stays nice and descriptive. Look:

func add(number firstNumber: Int, to secondNumber: Int) -> Int {
    return firstNumber + secondNumber
    // That's much better!
}

add(number: 3, to: 9)

// That's still a little messy. What if we didn't want any label for the first argument?

func add(_ firstNumber: Int, to secondNumber: Int) -> Int {
    return firstNumber + secondNumber
}

add(3, to: 9)

// Now it truly reads like a sentence: "add three to nine"! This is the way most functions are written in Swift.



// --------------------------------------------------------------------------------------------------------------------
// FUNCTIONS AS VALUES                                                                   // MARK: - Functions as Values
// --------------------------------------------------------------------------------------------------------------------

// This is a good time to talk about one of the other aspects of functions. Some people consider this a bit advanced,
// but I think you can handle it just fine. I won't go into it in too much detail (I hope), but I think it's important
// to start thinking about functions from this perspective early on, so bear with me. Don't worry about understanding
// it all now, just read through and soak it in a bit.

// There is one sense in which a function is a scrap of code that transforms some input(s) into some output(s). That
// transformation could be mathematical...

func multiply(x: Int, by multiplier: Int) -> Int {
    return x * multiplier
}

//print(multiply(x: 2, by: 19))
// Uncomment this to try it! ^

// ...or it could be any number of other types of transformation, like `stringerino(word:)` above, or like this:

func hello(name: String) -> String {
    return "Hello, \(name)"
}

// The point is that the traditional view of a function is that it's just a list of steps for the computer to perform,
// and it's kept bundled up with a name (e.g. `hello(name:)`) for convenience to the programmer. It's also seen as being
// something that sits in one place in your code and is static - it doesn't move around or change after you compile your
// code.

// However, there is another way of thinking about what a function is, which has become increasingly popular recently
// (on a relative timescale). That view of a function is that a function is a THING - it is a value - that you can put
// into a variable, modify, and pass around your program.

// Watch this.

let greeter = hello(name:)

// `greeter` is a value! It's a constant, declared with `let`, that contains a function! That means that you could do
// anything with that value that you could do with a String or an Int. Its type is neither String nor Int though... it's
// (String) -> String! It's still a function. You can call it!

//print(type(of: greeter))
// Uncomment this to print the type ^

//print(hello(name: "Søren"))
//print(greeter("Lakshay"))
// Uncomment these to try calling the two functions ^

// `greeter` is a value that contains exactly the same function as the original, `hello(name:)`. You might be wondering,
// though: "Why?? Why on earth would you want to do this, Søren?"

// Well, watch this (and don't worry about understanding the syntax, that'll come with time):

func stringerinoify(function: @escaping (String) -> String) -> (String) -> Void {
    return { string in
        let result = function(string)
        stringerino(word: result)
    }
}

// This is the bit we care about:
let greeterino: (String) -> Void = stringerinoify(function: greeter)

// What I've just done is this: I defined a function, called `stringerinoify(function:)`, which takes another function
// as an argument. It then turns that function into a new function, which first calls the original function, and then
// sends the result to `stringerino(word:)`, which adds "erino" to the end and prints it out. All in one step! Watch:

//greeterino("Søren")
// Uncomment this to watch it in action ^

// Anyway, it's not necessary to fully understand all of this at this point. Just keep in mind that all this is out
// there - functions aren't necessarily just static scraps of code that sit in your program and can be run. They're also
// equally as much *values* that you can store and pass around, like other types such as String and Int.

// In the end, this means there are two different ways of defining a function that are very similar. One is the normal
// way, like this...

func oneWay(x: Int, y: Int) -> String {
    return "\(x)\(y)"
}

// ..and the other is the "function as a value" way, like this:

let anotherWay: (Int, Int) -> String = { x, y in
    return "\(x)\(y)"
}

// They're both equally valid, but you'd use one or the other in specific circumstances depending on what you need, and
// in general you'd still use the first, normal way.

// Remember how I mentioned the word "closure" really on in this tutorial (line 27), and said that it effectively meant
// the same thing as "function"? Well, `anotherWay` is a closure! There's a bit of subtlety to why it's called a
// closure, but I'll explain that later, in the classes tutorial. Effectively, a closure is a function that's stored in
// a variable/constant.

// The nice thing about closures, though, is that they can be created "inline" - that is, without even putting them in
// a variable.

// You know how you can either create a String and put it in a variable, like this...

let aString = "Hello there!"

// ...or just create it in the middle of a function call, like this?

//print("Oh wow cool")
// Uncomment to try ^

// Well, you can create a closure (and by extension a function) in the middle of a function call too, and it doesn't
// have to be stored anywhere else! One of the main places where this comes in really handy is when you want to apply
// some operation to every element in an array.

// Imagine you wanted to double every number in an array. Here's the array:

let numbers = [1, 1, 2, 3, 5, 8, 13, 21, 34]

// One way of doubling them all could be to use a function, like this:

func doubleBoringly(numbers: [Int]) -> [Int] {
    var newNumbers: [Int] = []
    for number in numbers {
        newNumbers.append(number * 2)
    }
    return newNumbers
}

let doubledTheBoringWay = doubleBoringly(numbers: numbers)

//print(doubledTheBoringWay)
// I think you're getting the hang of the uncommenting thing now, right? ^

// Okay, fine. That works. But what if you wanted to be COOLER about it?

let doubledTheCoolWay = numbers.map({ number in
    return number * 2
})

//print(doubledTheCoolWay)

// That does the same thing, but it's COOL.

// So how does it work? Well, that's not too hard to wrap your head around, with a bit of thought, if you understand
// what closures are. I called the function `map` on `numbers` (don't worry too much about the `numbers.map` syntax -
// again, that will make sense once you do classes). `map` takes an argument which is a closure (so it's a function) and
// then applies that function to each element in the array! The cool part is that I created that closure inline where I
// called the `map` function. The curly braces surround a closure, created right then and there, that takes an argument
// called `number` and returns `number * 2`. And that's it!

// Cool, right?
